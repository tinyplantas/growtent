# README #

Raspberry Pi Thermometer, Hygrometer, and Webcam Monitor

### Dependencies ###

* A Raspberry Pi running Raspian
* Ruby + Rails + rbenv [Installation guide here](http://www.iconoclastlabs.com/blog/ruby-on-rails-on-the-raspberry-pi-b-with-rbenv)
* [rbenv-sudo](https://github.com/dcarley/rbenv-sudo)
* debian motion [Installation Guide](http://pimylifeup.com/raspberry-pi-webcam-server/)
* [DHT Sensor FFI](https://github.com/chetan/dht-sensor-ffi) to read temp / humidity from sensor
* [Chartkick](http://chartkick.com) for temp / humidity charting
* Postgres 9.4 db

### Starting the App ###

Start Motion

```./motion/startmotion```

Stop Motion

```./motion/stopmotion```

Start Rails Server with rbenv-sudo bound to localhost

```rbenv sudo rails s -b 0.0.0.0```

Navigate to your pi's ip address in the browser, port 3000

```raspi.local:3000```