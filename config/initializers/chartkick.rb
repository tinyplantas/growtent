Chartkick.options = {
  width: '100%', 
  colors: ['#9A3D8F', '#2F7E02'], 
  library: { 
    pointSize: 0, 
    legend: {
      position: 'bottom'
    }, 
    vAxis: {
      ticks: [20, 30, 40, 50, 60, 70, 80]
    }
  }
}