class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.decimal :temp
      t.decimal :hum

      t.timestamps null: false
    end
  end
end
