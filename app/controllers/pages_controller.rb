class PagesController < ApplicationController
  def index

    # get current temp / hum values for immediate page load
    val = DhtSensor.read(4, 11)
    @temp = val.temp_f
    @hum = val.humidity

    # get sensor values from db
    values = Sensor.where(temp: 50..130, created_at: 24.hours.ago..Time.now)
    
    sensors = values.every_nth(10)

    @temps = [["date", "Temperature"]]
    @hums = [["date", "Humidity"]]
    
    # format sensor values to google charts array
    @temps += sensors.map {|s| [s.created_at, s.temp]}
    @hums += sensors.map {|s| [s.created_at, s.hum]}

  end

  def sensor
    
    # get current sensor values for /temp route
    # pages.coffee gets value every 30 seconds
    val = DhtSensor.read(4, 11)
    @temp = val.temp_f
    @hum = val.humidity
    
    # save sensor value to database
    sensor = Sensor.new
    sensor.temp = @temp
    sensor.hum = @hum
    sensor.save
    
    # no layout when ajaxing data
    render layout: false

  end
end
