# take sensor reading by hitting /sensor route 
getSensor = ->
  $.ajax
    url: '/sensor'
    cache: false
    success: (html) ->
      $('.temp-wrap').empty().append html

$(document).ready ->
  getSensor()

  # take sensor reading every 30 seconds
  setInterval () ->
    getSensor()
  , 30000
